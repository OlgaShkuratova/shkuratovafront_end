const form = document.getElementById('form');
const label = document.getElementById('label');
const input = document.getElementById('input');
const btn = document.getElementById('btn');
const time = document.getElementById('time');

function randomNumber() {
    let numbers = [];
    while (numbers.length < 4) {
        let number = Math.floor(Math.random() * 10);
        if (numbers.indexOf(number) === -1) {
            numbers.push(number);
        }
    }
    if (numbers[0] === 0) {
        numbers[0] = Math.floor((Math.random() * 9) + 1)
    }
    return numbers;
}

const numbers = randomNumber();
console.log(`Number: ${numbers.join('')}`);

btn.addEventListener('click', playGame);
let count = 0;
function playGame(e) {
    e.preventDefault();
    const p = document.getElementById('note');
    let val = document.querySelector('input').value;
    let arr = val.split('').map(el => +el);
    let repeat = false;
    let sheeps = [];
    let ram = [];
    count++;
    for (let i = 0; i < arr.length; i++) {
        for (let j = i + 1; j < arr.length; j++) {
            if (arr[i] === arr[j]) {
                repeat = true;
            }
        }
    }
    if (arr.length < 4 || arr.length > 4 || arr.length === 0 || arr[0] === 0 || repeat) {
        p.innerHTML = `Invalid entered number. Number must contain
                four different digits and can not start with 0.`;
    } else {
        for (let i = 0; i < numbers.length; i++) {
            for (let j = 0; j < numbers.length; j++) {
                if (numbers[i] == arr[j] && i == j) {
                    ram.push(numbers[i]);
                } else if (numbers[i] == arr[j]) {
                    sheeps.push(numbers[i]);
                }
            }
        }
        p.innerHTML = `You have: ${ram.length} ram(s) and ${sheeps.length} sheep(s)`;
    }
    if (ram.length === 4) {
        clearResult();
        timer(true);
        const form = document.createElement('form');
        const label = document.createElement('label');
        const input = document.createElement('input');
        const btn = document.createElement('button');
        const btnAgain = document.createElement('button');
        const p = document.createElement('p');
        const img = document.createElement('img');
        const finalElem = document.createElement('p');

        p.innerHTML = 'Congratulations! You have guessed the number and win the game!';
        finalElem.innerHTML = "You completed the game " + "<br> in " + count + ' step(s)' + "<br> in " + time.innerHTML;
        label.innerHTML = 'Please enter your username';
        btn.innerHTML = 'Submit username';
        btnAgain.innerHTML = 'Play again';

        img.setAttribute('src', 'img/sheep.gif');
        input.setAttribute('placeholder', 'Enter username');
        input.setAttribute('id', 'username');
        input.setAttribute('autocomplete', 'off');
        btn.setAttribute('type', 'submit');

        finalElem.classList.add('final');
        btnAgain.style.marginLeft = '35px';
        form.style.paddingBottom = '30px';

        document.body.append(img);
        document.body.append(finalElem);
        document.body.append(form);
        document.body.append(btnAgain);
        form.append(p);
        form.append(label);
        label.append(input);
        form.append(btn);

        btn.addEventListener('click', (e) => {
            e.preventDefault();
            const input = document.querySelector('input').value;
            localStorage.setItem(input, count);
            if (input.value !== '' || null) {
                for (let i = 0; i < localStorage.length; i++) {
                    const p = document.createElement('p');
                    p.innerHTML = 'Player: ' + localStorage.key(i) + ', steps: ' + localStorage.getItem(localStorage.key(i));
                    form.append(p);
                }
                count = 0;
            }
        });

        btnAgain.addEventListener('click', (e) => {
            e.preventDefault();
            window.location.reload();
        })
    }
}

function clearResult() {
    input.value = '';
    while (document.body.firstChild) {
        document.body.removeChild(document.body.firstChild);
    }
}

const timer = (() => {
    let second = 0;
    let minute = 0;
    let isTimerOn = false;
    let timerId;
    return (shouldTimerOff = false) => {
        if (shouldTimerOff) {
            clearInterval(timerId)
        }
        if (isTimerOn === false) {
            timerId = setInterval(() => {
                time.innerHTML = minute + " mins " + second + " secs";
                second++;
                if (second == 60) {
                    minute++;
                    second = 0;
                }
            }, 1000);
            isTimerOn = true;
        }
    }
})();

timer();