﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		self.clear = function jsConsoleClear() {
			textArea.innerText = '';
		}
		return self;
	}
	jsConsole = new createJsConsole("#console");

}).call(this);

function functabs(number) {
	let div = document.getElementsByClassName('block');
	for (let i = 0; i < div.length; i++) {
		div[i].classList.add("hidden");
		jsConsole.clear();
		document.getElementById('number1').value = '';
		document.getElementById('number2').value = '';
		document.getElementById('text').value = '';
		document.getElementById('word').value = '';
		document.getElementById('arr5').value = '';
		document.getElementById('arr6').value = '';
		document.getElementById('position').value = '';
		document.getElementById('arr7').value = '';
		document.getElementById('search').value = '';
	}
	document.getElementById('div' + number).classList.remove("hidden");
	document.getElementById('div' + number).classList.add("show");
}

// //   1

const btn1 = document.getElementById('button1');

btn1.addEventListener('click', () => {
	let num = jsConsole.read('#number1').split('').pop();
	let digitWord = ['zero', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine'];
	jsConsole.writeLine(digitWord[num]);
})

// //   2

const btn2 = document.getElementById('button2');

btn2.addEventListener('click', () => {
	let num = jsConsole.readInteger('#number2');
	jsConsole.writeLine(('' + num).split('').reverse().join(''));
})

//  //  3

const btn3 = document.getElementById('button3');
const check = document.getElementById('checkbox');

btn3.addEventListener('click', () => {

	let array = jsConsole.read('#text').split(' ');
	let element = jsConsole.read('#word');

	if (check.checked) {
		jsConsole.writeLine(wordCount(array.map(i => i.toLowerCase()), element.toLowerCase()));

	} else {
		jsConsole.writeLine(wordCount(array, element));
	}

	function wordCount(array, element) {
		if (arguments.length === 2) {
			let arr = [];
			let count = 0;
			let item = array.indexOf(element);
			while (item != -1) {
				arr.push(item);
				item = array.indexOf(element, item + 1);
				count++
			}
			return 'There are  ' + count + ' occurances of the word ' + '"' + element + '"';
		} else {
			throw new Error('Signature is not supported');
		}
	}
})

// //   4

const btn4 = document.getElementById('button4');

btn4.addEventListener('click', () => {
	let divNum = document.getElementsByTagName('div');
	jsConsole.write(divNum.length);
})

// //   5

const btn5 = document.getElementById('button5');
const test = document.getElementById('test');

btn5.addEventListener('click', () => {
	let arr = jsConsole.read('#arr5').split(',').map(Number);
	let num = jsConsole.readInteger('#search');

	function countEntries(arr, num) {
		return arr.filter(el => el === num).length;
	}
	jsConsole.writeLine(countEntries(arr, num));
})

test.addEventListener('click', () => {
	let arr = jsConsole.read('#arr5').split(',').map(Number);

	for (let i = 0; i < 10; i++) {
		arr.push(Math.round(Math.random() * 10));
	}
	jsConsole.writeLine(arr);

	let min = 0;
	let max = 9;
	let num = jsConsole.readInteger('#search');
	num = Math.floor(min + Math.random() * (max + 1 - min));
	jsConsole.writeLine('random number: ' + num);

	function countEntries(arr, num) {
		return arr.filter(el => el === num).length + ' times';
	}
	jsConsole.writeLine(countEntries(arr, num));
})

// // 6

const btn6 = document.getElementById('button6');

btn6.addEventListener('click', () => {
	let arr = jsConsole.read('#arr6').split(',').map(Number);
	let i = jsConsole.readInteger('#position');

	function findNeighbors(arr, i) {
		if (arr[i - 1] != undefined && arr[i + 1] != undefined) {

			let current = arr[i];

			if (current > arr[i - 1] && current > arr[i + 1]) {
				jsConsole.writeLine(true);
			} else {
				jsConsole.writeLine(false);
			}
		} else {
			jsConsole.writeLine('The position cannot be first or last element in the sequence!');
		}
	}
	findNeighbors(arr, i);
});

// // 7

const btn7 = document.getElementById('button7');

btn7.addEventListener('click', () => {
	let arr = jsConsole.read('#arr7').split(',').map(Number);

	function findNeighbors(arr) {
		let index = -1;
		for (let i = 0; i < arr.length; i++) {
			if (arr[i] > arr[i + 1] && arr[i] > arr[i - 1]) {
				index = i;
				break;
			}
		}
		return index;
	}
	jsConsole.writeLine(findNeighbors(arr));
})
