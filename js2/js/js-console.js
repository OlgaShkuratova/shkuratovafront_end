﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}
		return self;
	}
	jsConsole = new createJsConsole("#console");

}).call(this);

function functabs(number) {

	let div = document.getElementsByClassName('block');
	for (let i = 0; i < div.length; i++) {
		div[i].classList.add("hidden");
	}
	document.getElementById('div' + number).classList.remove("hidden");
	document.getElementById('div' + number).classList.add("show");
}

// //   1

const btn1 = document.getElementById('button1');

btn1.addEventListener('click', () => {
	for (let i = 0; i < 20; i++) {
		jsConsole.writeLine(i * 5);
	}
})

// //   2

const btn2 = document.getElementById('button2');

btn2.addEventListener('click', () => {
	let a = jsConsole.read('#string1');
	let b = jsConsole.read('#string2');
	function lexicCompare(a, b) {
		let charA = a.trim();
		let charB = b.trim();
		let count = Math.max(charA.length, charB.length);
		for (let index = 0; index < count; index++) {
			let dif = (charA.charCodeAt(index) || 0) - (charB.charCodeAt(index) || 0);
			if (dif !== 0) {
				return Math.sign(dif);
			}
		}
		return 0;
	};
	let result = lexicCompare(a, b);
	if (result > 0) {
		jsConsole.writeLine('The second array is earlier.');
	} else if (result < 0) {
		jsConsole.writeLine('The first array is earlier.');
	} else {
		jsConsole.writeLine('Both arrays are equal.');
	}

})

// //   3

const btn3 = document.getElementById('button3');

btn3.addEventListener('click', () => {
	let arr = jsConsole.read('#arr1').split(',').map(Number);
	jsConsole.writeLine('The input sequence is:' + '<br>' + arr);
	let result;
	let max = 0;
	for (let i = 0; i < arr.length; i++) {
		let count = 0;
		for (let j = i; j < arr.length; j++) {
			if (arr[i] === arr[j])
				++count;
			else
				break;
		}
		if (count > max) {
			max = count;
			result = arr[i];
		}
	}
	let newArr = new Array(max);
	jsConsole.writeLine('The maximum sequence is:' + '<br>' + newArr.fill(result));
})

// //   4

const btn4 = document.getElementById('button4');

btn4.addEventListener('click', () => {
	let arr = jsConsole.read('#arr2').split(',').map(Number);
	jsConsole.writeLine('The input sequence is:' + '<br>' + arr);
	let res = [];
	for (let i = 1; i < arr.length; i++) {
		if (arr[i] - arr[i - 1] == 1) {
			if (!res.length)
				res.push(arr[i - 1]);
			res.push(arr[i]);
		} else if (res.length) {
			break;
		}
	}
	jsConsole.writeLine('The maximum Increasing sequence is:' + '<br>' + res);
})

//  //  5

const btn5 = document.getElementById('button5');

btn5.addEventListener('click', () => {
	let inputArr = jsConsole.read('#arr3');
	let array = [];
	array = inputArr.split(',');
	function sort(array) {
		for (let i = 0; i < array.length; i++) {
			let min = i;
			for (let j = i + 1; j < array.length; j++) {
				if (array[j] < array[min]) {
					min = j;
				}
			}
			if (min != i) {
				let tmp = array[i];
				array[i] = array[min];
				array[min] = tmp;
			}
		}
		return array;
	}
	sort(array);
	jsConsole.writeLine(array);
})

// //   6

const btn6 = document.getElementById('button6');

btn6.addEventListener('click', () => {
	let inputArr = jsConsole.read('#arr4');
	let array = [];
	array = inputArr.split(',');
	let arr = [];
	for (let i = 0; i < array.length; i++) {
		let item = array[i];
		if (arr[item]) {
			arr[item]++
		} else {
			arr[item] = 1;
		}
	}
	let maxCount = 0;
	let maxString = "";
	for (let item in arr) {
		if (maxCount < arr[item]) {
			maxCount = arr[item];
			maxString = item;
		}
	}
	jsConsole.writeLine(res = maxString + '(' + maxCount + ' times)');
})

// //  7

const btn7 = document.getElementById('button7');

btn7.addEventListener('click', () => {
	let inputArr = jsConsole.read('#arr5');
	let array = [];
	array = inputArr.split(',').map(Number);
	array.sort(function (a, b) {
		return a - b;
	});
	jsConsole.writeLine('Sorted array: ' + array);
	let item = jsConsole.readInteger('#search');
	function search(array, item) {
		let start = 0;
		let end = array.length - 1;
		let middle;
		while (start < end) {
			middle = parseInt((start + end) / 2);
			if (item <= array[middle]) {
				end = middle;
			}
			else {
				start = middle + 1;
			}
		}
		if (array[start] === item) {
			jsConsole.writeLine('The index of ' + item + ' is ' + start);
		}
		else {
			return -1;
		}
	}
	search(array, item);
})

