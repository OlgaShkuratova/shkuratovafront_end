﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		self.clear = function jsConsoleClear() {
			textArea.innerText = '';
		}
		return self;
	}
	jsConsole = new createJsConsole("#console");

}).call(this);

function functabs(number) {
	let div = document.getElementsByClassName('block');
	for (let i = 0; i < div.length; i++) {
		div[i].classList.add("hidden");
		jsConsole.clear();
		document.getElementById('firstname').value = '';
		document.getElementById('lastname').value = '';
		document.getElementById('age').value = '';
	}
	document.getElementById('div' + number).classList.remove("hidden");
	document.getElementById('div' + number).classList.add("show");
}

// //   1

const btn = document.getElementById('button');

btn.addEventListener('click', () => {
	let firstName = jsConsole.read('#firstname');
	let lastName = jsConsole.read('#lastname');
	let age = jsConsole.read('#age');

	function Person(firstName, lastName, age) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.age = age;
	}

	Object.defineProperties(Person.prototype, {

		firstName: {
			get: function () {
				return this._firstName;
			},

			set: function (value) {
				this._firstName = value;
			}
		},

		lastName: {
			get: function () {
				return this._lastName;
			},

			set: function (value) {
				this._lastName = value;
			}
		},

		age: {
			get: function () {
				return this._age;
			},

			set: function (value) {
				this._age = value;
			}
		},

		fullname: {
			get: function () {
				return this._firstName + ' ' + this._lastName;
			},

			set: function (value) {
				let split = value.split(' ');
				this._firstName = split[0];
				this._lastName = split[1];
			}
		},

		introduce: {
			get: function () {
				if (this._firstName === '' || this._lastName === '' || !this._age) {
					jsConsole.writeLine('Please fill in all fields');
				} else if (this._firstName instanceof String === false && typeof this._firstName !== "string" || this._firstName.length < 3 || this._firstName.length > 20 || !/^[a-zçéäëïöü'’‘–—-]{3,20}$/i.test(this._firstName) ||
					this._lastName instanceof String === false && typeof this._lastName !== "string" || this._lastName.length < 3 || this._lastName.length > 20 || !/^[a-zçéäëïöü'’‘–—-]{3,20}$/i.test(this._lastName)) {
					jsConsole.writeLine('Incorrect name');
				} else if (this._age === 'null' || this._age === 'false') {
					jsConsole.writeLine('Hello! My name is ' + this.fullname + ' and I am ' + 0 + ' - years - old');
				} else if (this._age === 'true') {
					jsConsole.writeLine('Hello! My name is ' + this.fullname + ' and I am ' + 1 + ' - years - old');
				} else if (this._age < 0 || this._age > 150 || /\D/i.test(this._age)) {
					jsConsole.writeLine('Incorrect age');
				} else {
					jsConsole.writeLine('Hello! My name is ' + this.fullname + ' and I am ' + this._age + ' - years - old')
				}
			}
		}
	});
	
	const person = new Person(firstName, lastName, age);

	person.introduce;
})



