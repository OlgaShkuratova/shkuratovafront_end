const array = document.getElementById("array");
const maxNum = document.getElementById("max-num");
const minNum = document.getElementById("min-num");
const total = document.getElementById("total");
const average = document.getElementById("average");
const textarea = document.getElementById("textarea");
const btn = document.getElementById("button");

btn.addEventListener("click", () => {
  arr = [];
  min = 1;
  max = 100;

  arr.length = array.value;
  for (let i = 0; i < arr.length; i++) {
    arr[i] = Math.floor(Math.random() * (max - min + 1)) + min;
  }
  console.log(arr);
  maxNum.value = Math.max(...arr);
  minNum.value = Math.min(...arr);
  total.value = arr.reduce((a, b) => a + b);
  average.value = arr.reduce((a, b) => (a + b)) / arr.length;
  textarea.value = arr.filter(el => el % 2 !== 0);
})

