﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		self.clear = function jsConsoleClear() {
			textArea.innerText = '';
		}
		return self;
	}
	jsConsole = new createJsConsole("#console");

}).call(this);

function functabs(number) {
	let div = document.getElementsByClassName('block');
	for (let i = 0; i < div.length; i++) {
		div[i].classList.add("hidden");
		jsConsole.clear();
		document.getElementById('text').value = '';
		document.body.style.backgroundColor = 'black';
	}
	document.getElementById('div' + number).classList.remove('hidden');
	document.getElementById('div' + number).classList.add('show');
}

// //   1

const btnTag = document.getElementById('tagName');
const btnQuery = document.getElementById('querySelector');

btnTag.addEventListener('click', getDivsByTag);
btnQuery.addEventListener('click', getDivsByQuery);

function getDivsByTag() {
	const divs = document.getElementsByTagName('div');
	for (let i = 3; i < 5; i++) {
		divs[i].style.borderColor = "red";
	}
}

function getDivsByQuery() {
	const divs = document.querySelectorAll('div');
	for (let i = 3; i < 5; i++) {
		divs[i].style.borderColor = "green";
	}
}

// //   2

const btn2 = document.getElementById('button2');

btn2.addEventListener('click', getValue);

function getValue() {
	const text = document.getElementById('text').value
	console.log(text);
}

// // //   3

const btn3 = document.getElementById('button3');

btn3.addEventListener('click', getColor);

function getColor() {
	const color = document.getElementById('color');
	document.body.style.backgroundColor = color.value;
}

// // //   4

const create = document.getElementById('create');
const animate = document.getElementById('animate');
const JSconsole = document.getElementById('console');
const CenterX = 400;
const CenterY = 450;
const radius = 150;
const input = document.getElementById('numDivs');
let numDivs;
let circle = [];

create.addEventListener('click', createDivs);
animate.addEventListener('click', moveDivs);

function createDivs() {
	numDivs = +input.value;
	for (let i = 0; i < numDivs; i++) {
		const div = document.createElement("div");
		div.setAttribute('class', 'circle');
		div.setAttribute('id', `circle${i}`);
		circle.push(div);
		let x = (CenterX + radius * Math.cos(2 * Math.PI * i / numDivs));
		let y = (CenterY + radius * Math.sin(2 * Math.PI * i / numDivs));
		div.style.left = x + 'px';
		div.style.top = y + 'px';
		JSconsole.appendChild(div);
	}
}

function moveDivs() {
	let init = 0;
	let angle = 2 * Math.PI / 30;
	setInterval(function () {
		for (let i = 0; i < numDivs; i++) {
			init += angle;
			let x = Math.floor(CenterX + radius * Math.cos(init));
			let y = Math.floor(CenterY + radius * Math.sin(init));
			circle[i].style.left = x + 'px';
			circle[i].style.top = y + 'px';
		}
	}, +speed.value)
}