﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		self.clear = function jsConsoleClear() {
			textArea.innerText = '';
		}
		return self;
	}
	jsConsole = new createJsConsole("#console");

}).call(this);

function functabs(number) {
	let div = document.getElementsByClassName('block');
	for (let i = 0; i < div.length; i++) {
		div[i].classList.add("hidden");
		jsConsole.clear();
		document.getElementById('text').value = '';
		document.getElementById('text2').value = '';
		document.getElementById('text3').value = '';
		document.getElementById('text4').value = '';
		document.getElementById('text5').value = '';
	}
	document.getElementById('div' + number).classList.remove('hidden');
	document.getElementById('div' + number).classList.add('show');
}

// //   1

const btn1 = document.getElementById('button1');

btn1.addEventListener('click', () => {
	const text = jsConsole.read('#text');

	function extractText() {
		let html = text.replace(/<(\/?[^>]+)>/g, '');
		jsConsole.writeLine(html);
	}
	extractText();
});

// //   2

const btn2 = document.getElementById('button2');

btn2.addEventListener('click', () => {
	const text = jsConsole.read('#text2');

	function parseUrl() {
		const url = new URL(text);

		const newUrl = JSON.parse(JSON.stringify({
			protocol: url.protocol,
			server: url.host,
			resourse: url.pathname
		}));
		jsConsole.writeLine('protocol: ' + newUrl.protocol + '<br>' + ' server: ' + newUrl.server + '<br>' + ' resource: ' + newUrl.resourse);
	}
	parseUrl();
});

// //   3

const btn3 = document.getElementById('button3');

btn3.addEventListener('click', () => {
	const text = jsConsole.read('#text3');

	function changeUrl() {
		let regex = /<a href="(.*?)">(.*?)<\/a>/g;
		let url = text.replace(regex, '[URL=$1]$2[/URL]');
		jsConsole.writeLine(url);
	}
	changeUrl();
});

// //   4

const btn4 = document.getElementById('button4');

btn4.addEventListener('click', () => {
	const text = jsConsole.read('#text4');

	function extractEmail() {
		let regex = /[a-zA-Z0-9]+@[a-zA-Z0-9]+\.[a-zA-Z]{2,4}/gi;
		let email = text.match(regex);
		email.forEach(el => jsConsole.writeLine(el));
	}
	extractEmail();
});

// //   5

const btn5 = document.getElementById('button5');

btn5.addEventListener('click', () => {
	const text = jsConsole.read('#text5');

	function isPalindrome() {
		item = text.split(' ').map(el => el.replace(/[,";:'. ]/g, ""));
		item.forEach(el => {
			if (el.split("").reverse().join("") === el && el !== "") {
				jsConsole.writeLine(el);
			}
		})
	}
	isPalindrome();
});