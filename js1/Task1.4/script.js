const a = document.getElementById("first-num");
const b = document.getElementById("second-num");
const sum = document.getElementById("sum");
const textarea = document.getElementById("textarea");
const btn = document.getElementById("button");

btn.addEventListener("click", () => {
    if (parseInt(a.value) < parseInt(b.value)) {
        let i = parseInt(a.value);
        let summa = 0;
        let list = "";
        while (i <= parseInt(b.value)) {
            summa += i;
            if (i % 2 !== 0) {
                list += " " + i;
            }
            i++;
        }
        sum.value = summa;
        if (list.length > 0) {
            textarea.value = list;
        }
    }
})
