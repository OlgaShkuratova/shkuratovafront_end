﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		self.clear = function jsConsoleClear() {
			textArea.innerText = '';
		}
		return self;
	}
	jsConsole = new createJsConsole("#console");

}).call(this);

function functabs(number) {
	let div = document.getElementsByClassName('block');
	for (let i = 0; i < div.length; i++) {
		div[i].classList.add("hidden");
		jsConsole.clear();
		document.getElementById('string1').value = '';
		document.getElementById('string2').value = '';
		document.getElementById('str1').value = '';
		document.getElementById('str2').value = '';
		document.getElementById('number').value = '';
		document.getElementById('string5').value = '';
		document.getElementById('string6').value = '';
	}
	document.getElementById('div' + number).classList.remove("hidden");
	document.getElementById('div' + number).classList.add("show");
}

// //   1

const btn1 = document.getElementById('button1');

btn1.addEventListener('click', () => {
	conc();
	function conc() {
		let a = jsConsole.read('#string1');
		let b = jsConsole.read('#string2');
		let res = a.concat(b);
		jsConsole.writeLine(res);
	}
})

// //   2

const btn2 = document.getElementById('button2');

btn2.addEventListener('click', () => {
	const comp = function () {
		let a = jsConsole.read('#str1');
		let b = jsConsole.read('#str2');
		jsConsole.writeLine(a === b ? 1 : -1);
	}
	comp();
})

// //   3

const btn3 = document.getElementById('button3');

btn3.addEventListener('click', () => {
	jsConsole.writeLine('message in console');
})

// //   4

const btn4 = document.getElementById('button4');

btn4.addEventListener('click', () => {
	let num = jsConsole.readInteger('#number');
	const func = function fib(n) {
		let a = 1;
		let b = 1;
		for (let i = 3; i <= n; i++) {
			let c = a + b;
			a = b;
			b = c;
		}
		return b;
	}
	jsConsole.writeLine(func(num));
})

// //   5

const btn5 = document.getElementById('button5');

btn5.addEventListener('click', () => {
	let str = jsConsole.read('#string5');
	function checkSpam() {
		jsConsole.writeLine(/spam|sex/i.test(str))
	}
	checkSpam();
})

// //   6

const btn6 = document.getElementById('button6');

btn6.addEventListener('click', () => {
	function checkLength() {
		let maxLength = 20;
		let str = jsConsole.read('#string6');
		if (str.length <= maxLength) {
			return str;
		}
		str = str.slice(0, maxLength);
		str += "...";

		jsConsole.writeLine(str);
	}
	checkLength();
})