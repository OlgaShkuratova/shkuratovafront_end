const slides = document.getElementsByClassName("item");
const item = document.getElementsByClassName("slider-item");

let index = 1;
let i;

const showSlides = (n) => {
    if (n > slides.length) {
        index = 1
    }
    if (n < 1) {
        index = slides.length
    }
    for (i = 0; i < slides.length; i++) {
        slides[i].style.display = "none";
    }
    for (i = 0; i < item.length; i++) {
        item[i].className = item[i].className.replace(" active", "");
    }
    slides[index - 1].style.display = "block";
    item[index - 1].className += " active";
}
const nextSlide = () => {
    showSlides(index += 1);
}

const previousSlide = () => {
    showSlides(index -= 1);
}

const currentSlide = (n) => {
    showSlides(index = n);
}
showSlides(index);