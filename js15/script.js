function functabs(number) {
    let div = document.getElementsByClassName('block');
    for (let i = 0; i < div.length; i++) {
        div[i].classList.add("hidden");
        while (task.firstChild) {
            task.removeChild(task.firstChild);
        }
    }
    document.getElementById('div' + number).classList.remove('hidden');
    document.getElementById('div' + number).classList.add('show');
}

// Task 1

document.getElementById('canvasTask')
    .addEventListener('click', canvasTask);

function canvasTask() {
    const canvas = document.getElementById('canvas');
    const ctx = canvas.getContext('2d');

    function drawHead() {

        // Face
        ctx.beginPath();
        ctx.fillStyle = "#90cad7";
        ctx.strokeStyle = "#22545f";
        ctx.lineWidth = '3';
        ctx.scale(1, 0.9);
        ctx.arc(100, 150, 60, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
        ctx.closePath();

        // Eyes
        ctx.beginPath();
        ctx.ellipse(65, 130, 9, 7, 0, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.ellipse(115, 130, 9, 7, 0, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.fillStyle = "#22545f";
        ctx.ellipse(63, 130, 2, 6, 0, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
        ctx.closePath();
        ctx.beginPath();
        ctx.ellipse(113, 130, 2, 6, 0, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();
        ctx.closePath();

        //Nose
        ctx.beginPath();
        ctx.moveTo(88, 130);
        ctx.lineTo(75, 160);
        ctx.lineTo(88, 160);
        ctx.stroke();
        ctx.closePath();

        //Mouth
        ctx.beginPath();
        ctx.ellipse(90, 180, 8, 23, 1.7, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.closePath();

        //Hat
        ctx.beginPath();
        ctx.fillStyle = '#396693';
        ctx.strokeStyle = '#000';
        ctx.lineWidth = '4';
        ctx.ellipse(100, 100, 75, 20, 0, 0, Math.PI * 2, false);
        ctx.stroke();
        ctx.fill();
        ctx.beginPath();
        ctx.lineWidth = '3';
        ctx.ellipse(100, 30, 40, 12, 0, 0, Math.PI * 2, true);
        ctx.moveTo(60, 30);
        ctx.lineTo(60, 90);
        ctx.moveTo(140, 30);
        ctx.lineTo(140, 90);
        ctx.fillRect(60, 30, 80, 60);
        ctx.fill();
        ctx.stroke();
        ctx.beginPath();
        ctx.ellipse(100, 90, 40, 12, 0, 0, Math.PI, false);
        ctx.fill();
        ctx.stroke();
    }

    function drawBike() {
        ctx.beginPath();
        ctx.fillStyle = "#90cad7";
        ctx.strokeStyle = "#337d8f";
        ctx.lineWidth = '3';
        ctx.arc(130, 350, 60, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(350, 350, 60, 0, 2 * Math.PI);
        ctx.fill();
        ctx.stroke();

        ctx.beginPath();
        ctx.lineWidth = '3';
        ctx.moveTo(350, 350);
        ctx.lineTo(330, 240);
        ctx.lineTo(280, 250);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(330, 240);
        ctx.lineTo(370, 200);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(210, 325);
        ctx.lineTo(270, 370);
        ctx.stroke();
        ctx.clearRect(220, 335, 35, 35);
        ctx.closePath();

        ctx.beginPath();
        ctx.moveTo(130, 350);
        ctx.lineTo(240, 350);
        ctx.lineTo(334, 270);
        ctx.lineTo(200, 270);
        ctx.lineTo(130, 350);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(240, 350);
        ctx.lineTo(190, 250);
        ctx.stroke();

        ctx.beginPath();
        ctx.moveTo(164, 250);
        ctx.lineTo(215, 250);
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(240, 348, 20, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.closePath();
    }

    function drawHouse() {
        //House
        ctx.beginPath();
        ctx.rect(60, 650, 405, 295);
        ctx.fillStyle = '#975b5b';
        ctx.strokeStyle = '#000';
        ctx.lineWidth = '4';
        ctx.fill();
        ctx.stroke();

        //Windows
        ctx.beginPath();
        ctx.rect(90, 690, 140, 85);
        ctx.fillStyle = '#000';
        ctx.fill();
        ctx.beginPath();
        ctx.moveTo(90, 733);
        ctx.lineTo(250, 733);
        ctx.strokeStyle = '#975b5b';
        ctx.lineWidth = '4';
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(162, 690);
        ctx.lineTo(162, 796);
        ctx.stroke();

        ctx.beginPath();
        ctx.rect(295, 690, 140, 85);
        ctx.fillStyle = '#000';
        ctx.fill();
        ctx.beginPath();
        ctx.moveTo(295, 733);
        ctx.lineTo(455, 733);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(367, 690);
        ctx.lineTo(367, 796);
        ctx.stroke();

        ctx.beginPath();
        ctx.rect(295, 815, 140, 85);
        ctx.fillStyle = '#000';
        ctx.fill();
        ctx.beginPath();
        ctx.moveTo(295, 857);
        ctx.lineTo(455, 857);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(367, 814);
        ctx.lineTo(367, 920);
        ctx.stroke();

        //Roof
        ctx.beginPath();
        ctx.moveTo(60, 650);
        ctx.lineTo(465, 650);
        ctx.lineTo(244, 384);
        ctx.lineTo(60, 650);
        ctx.strokeStyle = '#000';
        ctx.lineJoin = 'round';
        ctx.fillStyle = '#975b5b';
        ctx.fill();
        ctx.stroke();

        //Chimney
        ctx.beginPath();
        ctx.moveTo(329, 575);
        ctx.lineTo(329, 450);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(375, 575);
        ctx.lineTo(375, 450);
        ctx.stroke();
        ctx.fillRect(331, 450, 42, 125);
        ctx.fill();
        ctx.beginPath();
        ctx.ellipse(352, 449, 22, 7, 0, 0, 2 * Math.PI);
        ctx.lineWidth = '5';
        ctx.stroke();
        ctx.fill();

        //Door
        ctx.beginPath();
        ctx.lineWidth = '3';
        ctx.strokeStyle = '#000';
        ctx.moveTo(110, 945);
        ctx.lineTo(110, 860);
        ctx.ellipse(163, 860, 53, 40, 0, Math.PI, 0, false);
        ctx.moveTo(216, 860);
        ctx.lineTo(216, 945);
        ctx.stroke();
        ctx.beginPath();
        ctx.moveTo(163, 820);
        ctx.lineTo(163, 945);
        ctx.stroke();

        ctx.beginPath();
        ctx.arc(145, 900, 6, 0, 2 * Math.PI);
        ctx.stroke();
        ctx.beginPath();
        ctx.arc(179, 900, 6, 0, 2 * Math.PI);
        ctx.stroke();

    }

    drawHead();
    drawBike();
    drawHouse();

}

// Task 2

document.getElementById('trashTask')
    .addEventListener('click', trashTask);

function trashTask() {
    const div = document.createElement('div');
    const img = document.createElement('img');
    const input = document.createElement('input');
    const btn = document.createElement('button');
    input.setAttribute('placeholder', 'Enter number:')
    img.setAttribute('src', 'img/closeBin.png');
    img.setAttribute('class', 'trash');
    btn.innerHTML = 'Throw';
    task.append(input);
    task.append(btn);
    task.append(img);
    task.append(div);

    btn.addEventListener('click', run);

    function run(e) {
        e.preventDefault();
        const input = document.querySelector('input');
        const val = +input.value;

        function generateRandom() {
            let x = Math.round(Math.random() * 90);
            let y = Math.round(Math.random() * 90);
            return {
                x: x,
                y: y,
            }
        }

        function setPaper() {
            for (let i = 0; i < val; i++) {
                const paper = document.createElement('img');
                paper.setAttribute('src', 'img/paper.png');
                paper.setAttribute('class', `paper`);
                paper.setAttribute('data-img', `${i + 1}`);
                let x = generateRandom().x;
                let y = generateRandom().y;
                paper.style.left = `${x}%`;
                paper.style.top = `${y}%`;
                div.appendChild(paper);
            }
        }
        setPaper();

        const paper = document.querySelectorAll('.paper');
        const trash = document.querySelector('.trash');
        paper.forEach(item => {
            item.addEventListener('dragstart', dragstart);
            item.addEventListener('dragend', dragend);
        });

        function dragover(e) {
            e.preventDefault();
        }

        function dragenter() {
            this.src = 'img/openBin.png';
        }

        function dragleave() {
            this.src = 'img/closeBin.png';
        }

        function dragdrop(e) {
            e.preventDefault();
            const dragFlag = e.dataTransfer.getData('dragItem');
            const dragItem = document.querySelector(`[data-img="${dragFlag}"]`);
            dragItem.remove();
            this.src = 'img/closeBin.png';
        }

        function dragstart(e) {
            e.dataTransfer.setData('dragItem', this.dataset.img);
            setTimeout(() => {
                this.classList.add('hidden');
            }, 0)
        }

        function dragend(e) {
            e.preventDefault();
            this.classList.remove('hidden');
        }

        trash.addEventListener('dragover', dragover);
        trash.addEventListener('dragenter', dragenter);
        trash.addEventListener('drop', dragdrop);
        trash.addEventListener('dragleave', dragleave);
    }
}