let arr = new Array();
let max = 9;
let min = -9;
document.write("Исходный массив:" + "<br>");

for (let i = 0; i < 5; i++) {
    arr[i] = new Array();
    for (let j = 0; j < 5; j++) {
        arr[i][j] = Math.floor(Math.random() * (max - min + 1) + min);
        document.write(" " + arr[i][j] + " ");
    }
    document.write("<br/>");
}

document.write("<br/>" + "Полученный массив:" + "<br>");

for (let i = 0; i < arr.length; i++) {
    for (let j = 0; j < arr[i].length; j++) {
        if (i == j) {
            if (arr[i][j] < 0)
                arr[i][j] = "0"
            else
                arr[i][j] = "1";
        }
        document.write(" " + arr[i][j] + " ");
    }
    document.write("<br/>");
}
