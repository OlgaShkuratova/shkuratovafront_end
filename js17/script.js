const get = document.getElementById('get');
const post = document.getElementById('post');
const url = '01.data.json';

function makeRequest(method, url, body) {
    return new Promise(function (resolve, reject) {
        if (method === 'POST') {
            resolve(body);
        } else {
            let xhr = new XMLHttpRequest();
            xhr.open(method, url);
            xhr.onload = function () {
                if (this.status >= 200 && this.status < 300) {
                    resolve(xhr.response);
                } else {
                    reject({
                        status: this.status,
                        statusText: xhr.statusText
                    });
                }
            };
            xhr.onerror = function () {
                reject({
                    status: this.status,
                    statusText: xhr.statusText
                });
            };
            xhr.send(body);
        }
    });
}

get.addEventListener('click', getJSON);
post.addEventListener('click', postJSON);

function getJSON() {
    makeRequest('GET', url)
        .then(function (data) {
            document.getElementsByClassName('data')[0].innerHTML = data.toString();
        })
        .catch(function (err) {
            console.error('Error', err.statusText);
        });
}

function postJSON() {
    let form = document.forms.postForm;
    form.classList.add('show');
    post.addEventListener('click', () => {
        makeRequest('POST', '/', {
            'firstName': form.elements.firstName.value,
            'lastName': form.elements.lastName.value,
            'age': form.elements.age.value,
        })
            .then(function (data) {
                document.getElementsByClassName('data')[1].innerHTML = JSON.stringify(data);
            });
        form.classList.remove('show');
    });
}
