const num = document.getElementById("num");
const res = document.getElementById("result");
const btn = document.getElementById("button");

btn.addEventListener("click", () => {
  let factorial = 1;
  let i = parseInt(num.value);
  do {
    if (i == 0) {
      factorial = 1;
      break;
    }
    factorial = factorial * i;
    i -= 1;
  }
  while (i > 0);
  res.value = factorial;
})

