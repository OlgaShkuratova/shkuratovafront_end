﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		self.clear = function jsConsoleClear() {
			textArea.innerText = '';
		}
		return self;
	}
	jsConsole = new createJsConsole("#console");

}).call(this);

function functabs(number) {
	let div = document.getElementsByClassName('block');
	for (let i = 0; i < div.length; i++) {
		div[i].classList.add("hidden");
		jsConsole.clear();
		document.getElementById('array').value = '';
		document.getElementById('remove').value = '';
		document.getElementById('point1').value = '';
		document.getElementById('point2').value = '';
		document.getElementById('point3').value = '';
		document.getElementById('point4').value = '';
		document.getElementById('point5').value = '';
		document.getElementById('point6').value = '';
		document.getElementById('point7').value = '';
		document.getElementById('point8').value = '';
		document.getElementById('point9').value = '';
		document.getElementById('point10').value = '';
		document.getElementById('point11').value = '';
		document.getElementById('point12').value = '';
	}
	document.getElementById('div' + number).classList.remove('hidden');
	document.getElementById('div' + number).classList.add('show');
}

// //   1

const btn = document.getElementById('button1');

btn.addEventListener('click', () => {
	const p1 = jsConsole.readInteger('#point1');
	const p2 = jsConsole.readInteger('#point2');
	const p3 = jsConsole.readInteger('#point3');
	const p4 = jsConsole.readInteger('#point4');
	const p5 = jsConsole.readInteger('#point5');
	const p6 = jsConsole.readInteger('#point6');
	const p7 = jsConsole.readInteger('#point7');
	const p8 = jsConsole.readInteger('#point8');
	const p9 = jsConsole.readInteger('#point9');
	const p10 = jsConsole.readInteger('#point10');
	const p11 = jsConsole.readInteger('#point11');
	const p12 = jsConsole.readInteger('#point12');

	function createPoint(x, y) {
		return {
			x: x,
			y: y,
		};
	}

	function createLine(pointA, pointB) {
		return {
			pointA: pointA,
			pointB: pointB,
			getLine: function () {
				return 'Line point A(' + pointA.x + ',' + pointA.y + ')<br/>Line point B(' + pointB.x + ',' + pointB.y + ')';
			}
		};
	}

	function calcDistance(p1, p2) {
		return parseInt(Math.abs(p1.x - p2.x) * Math.abs(p1.x - p2.x)
			+ Math.abs(p1.y - p2.y) * Math.abs(p1.y - p2.y));
	}

	function canBeTriangle(l1, l2, l3) {
		const a = calcDistance(l1.pointA, l1.pointB);
		const b = calcDistance(l2.pointA, l2.pointB);
		const c = calcDistance(l3.pointA, l3.pointB);
		return (a + b > c) && (a + c > b) && (c + b > a) ? true : false;
	}

	const line1 = createLine(createPoint(p1, p2), createPoint(p3, p4));
	const line2 = createLine(createPoint(p5, p6), createPoint(p7, p8));
	const line3 = createLine(createPoint(p9, p10), createPoint(p11, p12));

	canBeTriangle(line1, line2, line3) ?
		jsConsole.writeLine('The lines CAN create triangle!' + '<br/>') :
		jsConsole.writeLine('<br/> The lines CANNOT create triangle!' + '<br/>');

	jsConsole.writeLine('Line 1: <br/>' + line1.getLine() + '<br/>');
	jsConsole.writeLine('Line 2: <br/>' + line2.getLine() + '<br/>');
	jsConsole.writeLine('Line 3: <br/>' + line3.getLine());
})

// //   2

const btn2 = document.getElementById('button2');

btn2.addEventListener('click', () => {
	const arr = jsConsole.read('#array').split(',');
	const item = jsConsole.read('#remove');

	Array.prototype.remove = function (n, arr) {
		return arr.filter(el => el !== n);
	}
	jsConsole.writeLine(arr.remove(item, arr));
})

// //   3

const btn3 = document.getElementById('button3');

btn3.addEventListener('click', () => {
	function deepCopy(ob) {
		let ob2;
		let i;

		if (typeof ob !== 'object' || !ob) {
			return ob;
		}
		if (Object.prototype.toString.apply(ob) === '[object Array]') {
			ob2 = [];
			for (i = 0; i < ob.length; i += 1) {
				ob2[i] = deepCopy(ob[i]);
			}
			return ob2;
		}
		ob2 = {}
		for (i in ob) {
			if (ob.hasOwnProperty(i)) {
				ob2[i] = deepCopy(ob[i]);
			}
		}
		return ob2;
	}
	
	let obj = {
		a: 1,
		b: 4,
		c: "Inessa",
		d: "Ksysha"
	};

	let obj2 = deepCopy(obj);

	function showObj(obj) {
		if (obj instanceof Object) {
			for (key in obj) {
				(obj[key] instanceof Object) ?
					showObj(obj[key]) :
					jsConsole.write(obj[key] + "</br>");
			}
		} else {
			jsConsole.write(obj + "</br>");
		}
	}

	showObj(obj2);
})

