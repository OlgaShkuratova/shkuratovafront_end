const div = document.createElement("div");
const mainDiv = document.createElement("div");
const p = document.createElement("p");
const form = document.createElement("form");
const input = document.createElement("input");

input.setAttribute("id", "input");
input.setAttribute("type", "text");

input.placeholder = "New item here...";
div.style.cssText = "border: 1px solid black;" +
    "padding: 5px;";

p.innerText = "Don't forget to:";
mainDiv.style.margin = "20px 0";

document.body.appendChild(div);
div.appendChild(p);
div.appendChild(mainDiv);
div.appendChild(form);
form.appendChild(input);
p.style.margin = "0";

function addItem(text) {
    const label = document.createElement("label");
    const input = document.createElement("input");
    const span = document.createElement("span");
    const inputValue = document.getElementById('input');
    input.setAttribute('type', 'checkbox');

    if (inputValue.value !== '') {
        span.innerText = inputValue.value;
        mainDiv.append(text);
        text.append(label);
        label.append(input);
        label.append(span);
    }
    inputValue.value = '';

}

function removeSelected() {
    const input = document.querySelectorAll('input[type=checkbox]');
    for (let i = 0; i < input.length; i++) {
        if (input[i].checked) {
            input[i].parentElement.remove();
        }
    }
}

function hideSelected() {
    const input = document.querySelectorAll('input[type=checkbox]');
    for (let i = 0; i < input.length; i++) {
        if (input[i].checked) {
            input[i].parentElement.style.display = 'none';
        }
    }
}
function showHidden() {
    const input = document.querySelectorAll('input[type=checkbox]');
    for (let i = 0; i < input.length; i++) {
        if (input[i].checked) {
            input[i].parentElement.style.display = 'block';
        }
    }
}

function appendButton(...arg) {
    for (let i = 0; i < arg.length; i++) {
        const btn = document.createElement("button");
        btn.style.margin = '2px';
        btn.innerHTML = arg[i];
        btn.value = arg[i];
        form.appendChild(btn);
    }
}

appendButton('Add', 'Remove selected', 'Hide selected', 'Show hidden');

form.addEventListener('click', (e) => {
    e.preventDefault();
    const div = document.createElement('div');
    div.style.marginLeft = "35px";
    if (e.target && e.target.nodeName === 'BUTTON' && e.target.value === 'Add') {
        addItem(div);
    } else if (e.target && e.target.nodeName === 'BUTTON'
        && e.target.value === 'Remove selected') {
        removeSelected();
    } else if (e.target && e.target.nodeName === 'BUTTON'
        && e.target.value === 'Hide selected') {
        hideSelected();
    } else if (e.target && e.target.nodeName === 'BUTTON'
        && e.target.value === 'Show hidden') {
        showHidden();
    }
});