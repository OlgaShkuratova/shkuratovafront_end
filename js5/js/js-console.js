﻿(function () {
	function createJsConsole(selector) {
		var self = this;
		var consoleElement = document.querySelector(selector);

		if (consoleElement.className) {
			consoleElement.className = consoleElement.className + " js-console";
		}
		else {
			consoleElement.className = "js-console";
		}

		var textArea = document.createElement("p");
		consoleElement.appendChild(textArea);

		self.write = function jsConsoleWrite(text) {
			var textLine = document.createElement("span");
			textLine.innerHTML = text;
			textArea.appendChild(textLine);
			consoleElement.scrollTop = consoleElement.scrollHeight;
		}

		self.writeLine = function jsConsoleWriteLine(text) {
			self.write(text);
			textArea.appendChild(document.createElement("br"));
		}

		self.read = function readText(inputSelector) {
			var element = document.querySelector(inputSelector);
			if (element.innerHTML) {
				return element.innerHTML;
			}
			else {
				return element.value;
			}
		}

		self.readInteger = function readInteger(inputSelector) {
			var text = self.read(inputSelector);
			return parseInt(text);
		}

		self.readFloat = function readFloat(inputSelector) {
			var text = self.read(inputSelector);
			return parseFloat(text);
		}

		self.clear = function jsConsoleClear() {
			textArea.innerText = '';
		}
		return self;
	}
	jsConsole = new createJsConsole("#console");

}).call(this);

function functabs(number) {
	let div = document.getElementsByClassName('block');
	for (let i = 0; i < div.length; i++) {
		div[i].classList.add("hidden");
		jsConsole.clear();
		document.getElementById('string1').value = '';
		document.getElementById('string2').value = '';
		document.getElementById('text').value = '';
		document.getElementById('text3').value = '';
		document.getElementById('word').value = '';
		document.getElementById('string4').value = '';
	}
	document.getElementById('div' + number).classList.remove("hidden");
	document.getElementById('div' + number).classList.add("show");
}

// //   1

const btn1 = document.getElementById('button1');

btn1.addEventListener('click', () => {

	function reverseStr() {
		let str = jsConsole.read('#string1').split('').reverse().join('');
		jsConsole.writeLine(str);
	}
	reverseStr();
})

// //   2

const btn2 = document.getElementById('button2');

btn2.addEventListener('click', () => {
	let str = jsConsole.read('#string2');

	function checkBrackets() {
		let count = 0;
		for (let i = 0; i <= str.length; i++) {
			if (str[i] == "(") {
				count++;
			} else if (str[i] == ")") {
				count--;
			}
		}
		if (count == 0 && str.match(/\(.+\)/)) {
			jsConsole.writeLine('Valid Expression!');
		} else {
			jsConsole.writeLine('Invalid Expression!');
		}
	}
	checkBrackets();
})

//  //  3

const btn3 = document.getElementById('button3');

btn3.addEventListener('click', () => {

	let string = jsConsole.read('#text3');
	let element = jsConsole.read('#word');

	function wordCount(string, element) {
		if (arguments.length === 2) {
			let arr = [];
			let count = 0;
			let item = string.indexOf(element);
			while (item != -1) {
				arr.push(item);
				item = string.indexOf(element, item + 1);
				count++
			}
			return 'The substring ' + '"' + element + '"' + ' is contained ' + count + ' times in the text.';
		} else {
			throw new Error('Signature is not supported');
		}
	}
	jsConsole.writeLine(wordCount(string.toLowerCase(), element.toLowerCase()));
})


// //   4

const btn4 = document.getElementById('button4');

btn4.addEventListener('click', () => {
	let str = jsConsole.read('#string4');
	console.log(str)
	function switchcase(str) {
		let res = str.replace(/<upcase>([\s\S]*?)<\/upcase>/g,
			a => a.toUpperCase())
			.replace(/<lowcase>([\s\S]*?)<\/lowcase>/g,
				a => a.toLowerCase())
			.replace(/<mixcase>([\s\S]*?)<\/mixcase>/g,
				a => [...a].map(el => Math.floor(Math.random() > 0.5) ? el.toLowerCase() : el.toUpperCase()).join(''));
		return res;
	}
	jsConsole.writeLine(switchcase(str));
})

// //   5

const btn5 = document.getElementById('button5');

btn5.addEventListener('click', () => {

	function replaceStr() {
		let str = jsConsole.read('#text').replace(/ /g, '&nbsp;');
		jsConsole.writeLine(str)
	}
	replaceStr();
})