const x1 = document.getElementById("first-num");
const x2 = document.getElementById("second-num");
const x3 = document.getElementById("third-num");
const res = document.getElementById("result");
const btn = document.getElementById("button");

btn.addEventListener("click", () => {
    res.value = (parseInt(x1.value) + parseInt(x2.value) + parseInt(x3.value)) / 3;
})

