const r = document.getElementById("radius");
const h = document.getElementById("number");
const volume = document.getElementById("volume");
const surface = document.getElementById("surface");
const btn = document.getElementById("button");

btn.addEventListener("click", () => {
    volume.value = Math.round(Math.PI * Math.pow(r.value, 2) * h.value);
    surface.value = Math.round(2 * Math.PI * r.value * (r.value + h.value));
})
